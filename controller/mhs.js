//================================================== START ROUTING ==============================================

module.exports = function (app) {
  //route untuk halaman mahasiswa
  app.get("/mhs", async (req, res) => {
    let sql = "SELECT * FROM tbl_mhs";
    let query = await conn.query(sql, (err, results) => {
      if (err) throw err;
      res.render("mhs_view", {
        results: results,
      });
    });
  });

  //route untuk insert data
  app.post("/mhs", async (req, res) => {
    let data = {
      nama_mhs: req.body.nama_mhs,
      jenis_kelamin: req.body.jk,
      email: req.body.email,
      agama: req.body.agama,
      alamat: req.body.alamat,
    };
    let sql = "INSERT INTO tbl_mhs SET ?";
    let query = await conn.query(sql, data, (err, results) => {
      if (err) throw err;
      res.redirect("/mhs");
    });
  });

  //route untuk update data
  app.post("/mhs/edit", async (req, res) => {
    let sql =
      "UPDATE tbl_mhs SET nama_mhs ='" +
      req.body.nama_mhs +
      "', jenis_kelamin ='" +
      req.body.jk +
      "', email ='" +
      req.body.email +
      "', agama ='" +
      req.body.agama +
      "', alamat ='" +
      req.body.alamat +
      "' WHERE id_mhs=" +
      req.body.id;
    let query = await conn.query(sql, (err, results) => {
      if (err) throw err;
      res.redirect("/mhs");
    });
  });

  //route untuk delete data
  app.post("/delete", (req, res) => {
    let sql = "DELETE FROM tbl_mhs WHERE id_mhs=" + req.body.id_mhs + "";
    let query = conn.query(sql, (err, results) => {
      if (err) throw err;
      res.redirect("/mhs");
    });
  });
};

//=================================================== END ROUTING ===============================================
