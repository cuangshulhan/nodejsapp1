//================================================== START SETUP ==============================================

//use path module
const path = require("path");
//use express module
const express = require("express");
const app = express();
//use hbs view engine
const hbs = require("hbs");
//use bodyParser middleware
const bodyParser = require("body-parser");
//use mysql database
conn = require("./config/koneksi");
//connect ke database
conn.connect((err) => {
  if (err) throw err;
  console.log("Mysql Connected...");
});

//set views file
app.set("views", path.join(__dirname, "views"));
//set view engine
app.set("view engine", "hbs");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder public sebagai static folder untuk static file
app.use("/assets", express.static(__dirname + "/public"));

app.listen(8000, () => {
  console.log("Server is running at port 8000");
});

//==================================================== END SETUP ===============================================

//============================================== START ROUTE CONTROLLER =============================================

let url_mhs = require("./controller/mhs")(app);

//============================================== END ROUTE CONTROLLER ===============================================

//================================================== START ROUTING ==============================================

//route untuk halaman home
app.get("/", (req, res) => {
  res.send("Welcome To Express");
});

//route untuk homepage
app.get("/about", (req, res) => {
  res.send("This is about page");
});

//=================================================== END ROUTING ===============================================
