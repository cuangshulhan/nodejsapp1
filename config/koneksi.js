//use mysql database
const mysql = require("mysql");

//konfigurasi koneksi
const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "nodejs_mahasiswa",
});

module.exports = conn;
